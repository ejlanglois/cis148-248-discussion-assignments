/*
    Name: Edmond Langlois
    Date: 2018-10-17

    This application will create a collection of objects.  User input will be used to create individual
    widget objects.

    Team member with branch:
*/

import java.util.Scanner;

public class WidgetFactory {
    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        Widget[] widgetList = new Widget[Widget.getMaxWidgets()];
        String name;
        double price;
        double sum = 0;

        while(Widget.getCount() < Widget.getMaxWidgets()){
            System.out.println("Please enter a new name:");
            name = scnr.nextLine();
            System.out.println("Please enter widget's price:");
            price = scnr.nextDouble();
            scnr.nextLine();
            widgetList[Widget.getCount()] = new Widget(name,price);
            Widget.updateCount();
        }

        System.out.println("List of widgets:");
        for(int i = 0; i < Widget.getMaxWidgets(); i++){
            System.out.println("Name: " + widgetList[i].getName() + " Price: " + widgetList[i].getPrice());
            sum += widgetList[i].getPrice();
        }
        System.out.println("Average Price: " + sum / Widget.getMaxWidgets());
        /*
            - Setup Scanner object to read user input
            - use a loop to
                - prompt user to input a widget name and price
                - store values in a new widget
                - add the widget to a widget array
                - update widget count
            - You can only create up to the maximum number of widgets allowed
            - Display the collection of widgets and the average price
         */
    }
}
