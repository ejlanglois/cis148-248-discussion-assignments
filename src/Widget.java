/*
    Name: Edmond Langlois
    Date: 2018-10-17

    This application will create a collection of objects.  User input will be used to create individual
    widget objects.

    Team member with branch:
*/

public class Widget {
    // Object fields
    private String name;
    private double price;

    // Class fields
    private final static int MAX_WIDGETS = 5;
    private static int count = 0;

    // Argument-based Constructor
    public Widget(String aName, double aPrice) {
        price = aPrice;
        name = aName;
    }

    // No-argument Constructor
    public Widget() {
        this("Generic",1.0);
    }

    // Object methods
    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice() {

    }

    // Class Methods
    public static int getMaxWidgets() {
        return MAX_WIDGETS;
    }

    public static int getCount() {
        return count;
    }

    public static void updateCount() {
        count += 1;
    }
}
